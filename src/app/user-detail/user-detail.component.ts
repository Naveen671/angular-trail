import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  @Input() items: [];
  newValue = '';
  @Output() newItems = new EventEmitter<string>()
  constructor() { }

  ngOnInit() {
  }

  onDrag(){
    this.newItems.emit(this.newValue);
  }
}
