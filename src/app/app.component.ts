import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string = 'first';
  rootName: string = 'Sirisha';
  newItems = ['apple', 'banana'];
  onChangeName(event){
  this.rootName = event;
  }
  onNewItems(event){
    this.newItems.push(event);
    console.log(this.newItems);
  }
}
