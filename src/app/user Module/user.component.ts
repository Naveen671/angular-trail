import {Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html'
})
export class UserComponent{
  @Input() newPerson: string;
  userInput: string = 'NAVEEN';
  @Output() newUserInput= new EventEmitter<string>();
  newState: string = 'loading';
  state: boolean = false;

onchange(event){
  // this.newPerson = event.target.value;
  this.newUserInput.emit(event.target.value);
}

onclick(){
  this.state = !this.state;
  if(this.state) {
    this.newState = 'loaded';
  }else{
    this.newState = 'crashes';
  }
}}
